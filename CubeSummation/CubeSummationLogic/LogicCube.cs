﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CubeSummationEntities;

namespace CubeSummationLogic
{
    /// <summary>
    /// Clase para el manejo de la capa logica
    /// </summary>
    public class LogicCube
    {
        private string[] arrayData;
       
        public LogicCube(string[] data)
        {
            arrayData = data;
        }
        /// <summary>
        /// Realiza la operación de update 
        /// </summary>
        /// <param name="dataOperation"></param>
        /// <param name="matriz"></param>
        /// <returns></returns>
        public int Update(List<int> dataOperation, int[,,] matriz)
        {           
            int x, y, z, w;

            x = dataOperation[0]-1;
            y = dataOperation[1]-1;
            z = dataOperation[2]-1;
            w = dataOperation[3];

            if (-1000000000 <= w && w <= 1000000000)
            {
                matriz[x, y, z] = w;
            }
            return matriz[x, y, z];
        }

        /// <summary>
        /// Realiza la operacion de query
        /// </summary>
        /// <param name="dataOperation"></param>
        /// <param name="matriz"></param>
        /// <returns></returns>

        public int Query(List<int> dataOperation, int[,,] matriz)
        {
            int sum=0;
            int x1, y1, z1, x2, y2, z2;

            x1 = dataOperation[0]-1;
            y1 = dataOperation[1]-1;
            z1 = dataOperation[2]-1;

            x2 = dataOperation[3]-1;
            y2 = dataOperation[4]-1;
            z2 = dataOperation[5]-1;

            for (int x = x1; x <= x2; x++)
            {
                for (int y = y1; y <= y2; y++)
                {
                    for (int z = z1; z <= z2; z++)
                    {
                        sum += matriz[x, y, z];
                    }
                }
            }
            return sum;
        }

        /// <summary>
        /// Identifica el tipo de operacion a realizar
        /// </summary>
        /// <param name="operationText"></param>
        /// <returns></returns>
        public Operation IdentifyOperation(string operationText)
        {
            Operation operation=new Operation();

            string[] operationData = operationText.Split(' ');
            string operationType = operationData[0];

            if (operationType.Equals("UPDATE",StringComparison.OrdinalIgnoreCase))
            {
                operation.OperationType = CubeSummationEntities.Enums.OperationType.Update;
            }
            else if (operationType.Equals("QUERY", StringComparison.OrdinalIgnoreCase))
            {
                operation.OperationType = CubeSummationEntities.Enums.OperationType.Query;
            }
            operation.OperationParameters = operationData.Skip(1).Take(operationData.Length-1).Select(o=>Convert.ToInt32(o)).ToList();

            return operation;
        }

        /// <summary>
        /// Invoca la operacion a realizar dependiendo del tipo
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="matriz"></param>
        /// <returns></returns>
        public int RunOperationCubeSummation(Operation operation,int[,,] matriz)
        {
            int result = 0;
           
            if (operation.OperationType==CubeSummationEntities.Enums.OperationType.Update)
            {
                result = Update(operation.OperationParameters,matriz);
            }
            else if (operation.OperationType==CubeSummationEntities.Enums.OperationType.Query)
            {
                result = Query(operation.OperationParameters, matriz);
            }
                        
            return result;
        }

        /// <summary>
        /// Invoca la ejecucion del calculo de las operaciones
        /// </summary>
        /// <returns></returns>
        public CubeSummationResult RunCubeSummation()
        {
            CubeSummationResult result = new  CubeSummationResult();

            try
            {
                int initialRow = 0;
                int testCases = Convert.ToInt32(arrayData[initialRow++]);
                Cube cube = null;

                if (testCases > 0 && testCases <= 50)
                {
                    for (int testCase = 0; testCase < testCases; testCase++)
                    {
                        int[] dataCase = arrayData[initialRow++].Split(' ').Select(n => Convert.ToInt32(n)).ToArray();
                        int operations = Convert.ToInt32(dataCase[1]);
                        int dimension = dataCase[0];
                        if ((dimension > 0 && dimension < 100) && (operations > 0 && operations < 1000))
                        {
                            cube = new Cube(dimension);

                            for (int operationNumber = 0; operationNumber < operations; operationNumber++)
                            {
                                string operationText = arrayData[initialRow++];

                                Operation operation = IdentifyOperation(operationText);
                                int operationResult = RunOperationCubeSummation(operation, cube.Matriz);

                                if (operation.OperationType == CubeSummationEntities.Enums.OperationType.Query)
                                {
                                    result.OperationsResults.Add(operationResult);
                                }
                            }
                        }
                        else
                        {
                            throw new Exception("La dimension ingresada y/o el número  de operaciones no son validas.");
                        }
                    }
                }
                else
                {
                    throw new Exception("El número de casos de prueba no es válido");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
             
            return result;
        }
    }
}