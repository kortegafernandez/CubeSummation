﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeSummationEntities.Enums
{
    public enum OperationType
    {
        Query, Update
    }
}
