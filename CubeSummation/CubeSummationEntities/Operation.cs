﻿using CubeSummationEntities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeSummationEntities
{
    /// <summary>
    /// Clase para manejar la operación
    /// </summary>
    public class Operation
    {
        public OperationType OperationType { get; set; }

        public List<int> OperationParameters { get; set; }

    }
}
