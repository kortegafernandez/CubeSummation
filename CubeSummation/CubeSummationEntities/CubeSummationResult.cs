﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeSummationEntities
{
    /// <summary>
    /// Clase para manejar el resultado de la operación
    /// </summary>
    public class CubeSummationResult
    {
        public CubeSummationResult()
        {
            OperationsResults = new List<int>();
        }
        public List<int> OperationsResults { get; set; }
    }
}
