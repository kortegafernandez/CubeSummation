﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeSummationEntities
{
    /// <summary>
    /// Clase para manejar la matriz 
    /// </summary>
    public class Cube
    {
        public Cube(int dimension)
        {
            Dimension = dimension;
            CreateMatriz();
        }
        //[Range(1, 100, ErrorMessage = "La dimension debe ser entre {0}  {1}")]
        public int Dimension { get; set; }
        public int[,,] Matriz { get; set; }

        private void CreateMatriz()
        {
            Matriz = new int[Dimension, Dimension, Dimension];
            for (int i = 0; i < Dimension; i++)
            {
                for (int j = 0; j < Dimension; j++)
                {
                    for (int k = 0; k < Dimension; k++)
                    {
                        Matriz[i, j, k] = 0;
                    }
                }
            }
        }

    }
}

