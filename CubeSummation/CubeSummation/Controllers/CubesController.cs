﻿using CubeSummationLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CubeSummationEntities;

namespace CubeSummation.Controllers
{
    /// <summary>
    /// Controlador para operaciones conla matriz
    /// </summary>
    public class CubesController : Controller
    {

        // GET: Cubes
        public ActionResult Index()
        {
            return View();
        }

       
        /// <summary>
        /// Invoca el metodo del calculo de las operaciones
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]       
        public ActionResult Index(string input)
        {
            try
            {
                string[] arrayData = System.Text.RegularExpressions.Regex.Split(input, "\r\n");

                LogicCube logic = new LogicCube(arrayData);

                CubeSummationResult result = logic.RunCubeSummation();
                return View(result);
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex,"Cubes","Index"));
            }
           

            
        }
    }
}